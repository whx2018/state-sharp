﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace StateSharp
{
    /// <summary>
    /// 转换类。
    /// </summary>
    public class Transition<TType>
    {
        TType m_type;
        /// <summary>
        /// 转换类型。
        /// </summary>
        public TType Type
        {
            get { return m_type; }
            set { m_type = value; }
        }
        State<TType> m_destState;
        /// <summary>
        /// 目标状态。
        /// </summary>
        public State<TType> DestState
        {
            get { return m_destState; }
            set { m_destState = value; }
        }
        public Transition()
        {
        }
        /// <summary>
        /// 转换类构造。
        /// </summary>
        /// <param name="type"></param>
        /// <param name="destState"></param>
        public Transition(TType type, State<TType> destState)
        {
            m_type = type;
            m_destState = destState;
        }
    }
}
