﻿using StateSharp;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace UnitTest
{


    /// <summary>
    ///这是 ContextTest 的测试类，旨在
    ///包含所有 ContextTest 单元测试
    ///</summary>
    [TestClass()]
    public class ContextTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///获取或设置测试上下文，上下文提供
        ///有关当前测试运行及其功能的信息。
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region 附加测试特性
        // 
        //编写测试时，还可使用以下特性:
        //
        //使用 ClassInitialize 在运行类中的第一个测试前先运行代码
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //使用 ClassCleanup 在运行完类中的所有测试后再运行代码
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //使用 TestInitialize 在运行每个测试前先运行代码
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //使用 TestCleanup 在运行完每个测试后运行代码
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion

        /// <summary>
        /// 测试多状态转换。
        /// </summary>
        [TestMethod()]
        public void DoTransitionMutiplyTest()
        {
            int value = 0;
            int currentTransition = 0;
            const int state1Value = 1;
            const int state2Value = 2;
            const int state3Value = 3;
            // 创建初始状态。 s1->s2  s1->s3 s3->s4
            State<int> state1 = new State<int>();
            State<int> state2 = new State<int>();
            State<int> state3 = new State<int>();
            State<int> state4 = new State<int>();

            state1.Exec = new Action<int>((t) => { value = state1Value; currentTransition = t; });
            state2.Exec = new Action<int>((t) => { value = state2Value; currentTransition = t; });
            state3.Exec = new Action<int>((t) => { value = state3Value; currentTransition = t; });

            state1.AddTransition(2, state2);
            state1.AddTransition(3, state3);
            state3.AddTransition(4, state4);

            Context<int> target = new Context<int>(); //   初始化为适当的值 
            target.InitState(state1);
            // 开始。
            target.Start();
            int trasition = 2;
            target.DoTransition(trasition); // 转换到s2
            Assert.AreEqual(state2Value, value); // state2被执行。
            Assert.AreEqual(target.CurrentState, state2);
            Assert.AreEqual(trasition, currentTransition);

            target.InitState(state1);
            // 开始。
            target.Start();
            trasition = 3;
            target.DoTransition(trasition); // 转换到s3
            Assert.AreEqual(state3Value, value); // state3被执行。
            Assert.AreEqual(target.CurrentState, state3);
            Assert.AreEqual(trasition, currentTransition);

            target.DoTransition(4); // 转换到s4
            Assert.AreEqual(state3Value, value); // state4被执行。但不改变值。
            Assert.AreEqual(target.CurrentState, state4);
        }
        /// <summary>
        /// 测试基本状态转换。
        /// </summary>
        [TestMethod()]
        public void DoTransitionTest()
        {
            int value = 0;
            const int state1Value = 1;
            const int state2Value = 2;

            // 创建初始状态。s1->s2 
            State<int> state1 = new State<int>();
            State<int> state2 = new State<int>();

            state1.Exec = new Action<int>((t) => { value = state1Value; });
            state2.Exec = new Action<int>((t) => { value = state2Value; });

            state1.AddTransition(2, state2);

            Context<int> target = new Context<int>(); //   初始化为适当的值  
            target.InitState(state1);
            // 开始。
            target.Start();
            Assert.AreEqual(state1Value, value); // state1被执行。
            Assert.AreEqual(target.CurrentState, state1);

            int transition = 1;
            bool ret = target.DoTransition(transition);
            Assert.AreEqual(ret, false);
            Assert.AreEqual(state1Value, value); // 未执行任何状态。
            Assert.AreEqual(target.CurrentState, state1);

            // state1 转换到state2。
            transition = 2;
            ret = target.DoTransition(transition);
            Assert.AreEqual(ret, true);
            Assert.AreEqual(state2Value, value); // 状态2被执行。
            Assert.AreEqual(target.CurrentState, state2);
        }
        /// <summary>
        /// 测试不通过的状态转换。
        /// </summary>
        [TestMethod]
        public void DoTransitionBeforeTest()
        {
            int value = 0;
            const int state1Value = 1;
            const int state2Value = 2;

            // 创建初始状态。s1->s2 
            State<int> state1 = new State<int>();
            State<int> state2 = new State<int>();

            state1.Exec = new Action<int>((t) => { value = state1Value; });
            state2.Exec = new Action<int>((t) => { value = state2Value; });
            bool trasitionAccess = false;// 能转换
            state2.Enter = (t) => { return trasitionAccess; }; // 不能转换

            state1.AddTransition(2, state2);

            Context<int> target = new Context<int>(); //   初始化为适当的值  
            target.InitState(state1);
            // 开始。
            target.Start();
            Assert.AreEqual(state1Value, value); // state1被执行。
            Assert.AreEqual(target.CurrentState, state1);

            int transition = 1;
            bool ret = target.DoTransition(transition);
            Assert.AreEqual(ret, false);
            Assert.AreEqual(state1Value, value); // 未执行任何状态。
            Assert.AreEqual(target.CurrentState, state1);

            // state1 转换到state2。
            transition = 2;
            ret = target.DoTransition(transition);
            Assert.AreEqual(ret, false);
            Assert.AreEqual(state1Value, value); // 未执行任何状态。

            trasitionAccess = true;
            ret = target.DoTransition(transition);
            Assert.AreEqual(ret, true);
            Assert.AreEqual(state2Value, value); // 状态2被执行。
            Assert.AreEqual(target.CurrentState, state2);
        }
    }
}
